# DC.pm-hello-world

A "hello world" project for demo'ing Docker containers and cloud technology for DC.pm

[DC.pm](http://dc.pm.org/)

## Build it

    docker build -t dcpm_hw .

## Run it

    docker run -p 3000:3000 dcpm_hw:latest

## Tag it

    docker tag dcpm_hw:latest us.icr.io/swagg_space/dcpm_hw

## Push it

    docker push us.icr.io/swagg_space/dcpm_hw
