FROM rakudo-star:2020.10

# Move it
WORKDIR /opt
COPY bin/ ./bin/
COPY views/ ./views/
COPY META6.json .

# Dependencies
RUN zef --depsonly install .

# Environment
ENV BAILADOR="host:0.0.0.0"
#EXPOSE 3000

# Send it
CMD ["bailador", "easy", "bin/dcpm_hw.raku"]
